<?php
    include_once('inc/header.php'); 
    include("lib/User.php");
    Session::checkSession();
    $user = new User();
?>

<?php
    include "lib/Product.php";
    $product = new Product();
?>

<?php
    if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['addProduct'])) {
        $addProduct = $product->addProduct($_POST, $_FILES);
    }
?>

<div class="panel-body">
	<!-- default navbar goes here -->
	<nav class="navbar navbar-default">
	    <div class="container-fluid">
			    <div class="navbar-header">
				    <span class="navbar-brand">
            <h4><a style="margin-top: -10px;" href="index.php" target="_blank" class="btn btn-default">Go to Home</a></h4>
          </span>
			    </div>
			    <ul class="nav navbar-nav pull-right">
				      <li><a><h4>Admin</h4></a></li>
			    </ul>
		  </div>
	</nav>

    <?php
        if (isset($addProduct)) {
            echo $addProduct;
        }
    ?>
		
	<!-- information table goes here -->
	<form action="" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="productID">Product ID</label>
            <input type="text" class="form-control" id="productID" name="product_id" placeholder="Product ID"/>
        </div>
        <div class="form-group">
            <label for="productName">Product Name</label>
            <input type="text" class="form-control" id="productName" name="product_name" placeholder="Product Name"/>
        </div>
        <div class="form-group">
            <label for="productManufacturingCost">Manufacturing Cost</label>
            <input type="number" class="form-control" id="exampleInputEmail1" name="product_manufacture" placeholder="Manufacturing Cost"/>
        </div>
        <div class="form-group">
            <label for="productPrice">Product Price</label>
            <input type="number" class="form-control" id="productPrice" name="product_price" placeholder="Product Price"/>
        </div>
        <!--
        <div class="form-group">
            <label for="productSell">Product Total Sell</label>
            <input type="number" class="form-control" id="productSell" name="product_sell" placeholder="Product Total Sell"/>
        </div>
        -->
        <div class="form-group">
            <label for="productImage">Product Image</label>
            <input type="file" id="productImage" name="image"/>
            <p class="help-block">Click "Choose File" button to upload product image</p>
        </div>
        <!--
        <div class="checkbox">
            <label>
                <input type="checkbox"> Check me out
            </label>
        </div>
        -->
        <button type="submit" class="btn btn-default" name="addProduct">Save Product</button>
    </form>
</div>


<?php
    include_once "inc/footer.php";
?>

