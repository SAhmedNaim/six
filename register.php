<?php
    include 'inc/header.php';
    include 'lib/User.php';
    Session::checkLogin();
?>

<?php
    $user = new User();
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['register'])) {
        $userRegistration = $user->userRegistration($_POST);
    }
?>

<div class="panel-heading">
    <h2>User Registration</h2>
</div>

<?php
    if (isset($userRegistration)) {
        echo $userRegistration;
    }
?>

<div class="panel-body">
    <div class="row">
        <div class="col-md-4">
            <img src="inc/img/logo.jpg" alt="Company Logo" style="height: 335px; width: 350px;" />
        </div>
        <div class="col-md-8">
            <div style="max-width: 600px; margin: 0px auto;">
                <form action="" method="post">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" id="username" name="username" class="form-control" placeholder="Type username here" style="min-width:100%" />
                    </div>
                    <div class="form-group">
                        <label for="email">Email Address</label>
                        <input type="text" id="email" name="email" class="form-control" placeholder="Type email here" style="min-width:100%" />
                    </div>
                    
                    <!-- status -->
                    <label for="status">Status</label>
                    <select name="status" class="form-control">
                        <option value="" selected="">Choose your status</option>
                        <option value="Admin">Admin</option>
                        <option value="Sells Person">Sells Person</option>
                    </select>
                    <br/>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Type password here" style="min-width:100%" />
                    </div>
                    <button type="submit" name="register" class="btn btn-default">Register</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
    include_once "inc/footer.php";
?>
