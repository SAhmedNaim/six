-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2017 at 08:51 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_six`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `order_id` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `customer_cell` varchar(255) NOT NULL,
  `customer_address` text NOT NULL,
  `shoulder` int(11) NOT NULL,
  `put` int(11) NOT NULL,
  `chest` int(11) NOT NULL,
  `length` int(11) NOT NULL,
  `hand` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `payment` int(11) NOT NULL,
  `total_cost` int(11) NOT NULL,
  `delivery_date` varchar(255) NOT NULL,
  `delivery_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`order_id`, `customer_name`, `customer_cell`, `customer_address`, `shoulder`, `put`, `chest`, `length`, `hand`, `quantity`, `payment`, `total_cost`, `delivery_date`, `delivery_order`) VALUES
(1, 'Alamgir Hossain', '01900000000', 'Banani, Dhaka.', 20, 16, 22, 32, 16, 1, 500, 500, '2017-09-06', 1),
(2, 'S Ahmed Naim', '01921289288', 'West Rajabazar, Dhanmondi, Dhaka.', 32, 12, 23, 34, 18, 1, 500, 500, '2017-09-07', 1),
(3, 'Nurul Huda Sarker', '01700000000', 'Banani, Dhaka.', 26, 24, 26, 30, 14, 1, 250, 350, '2017-09-16', 0),
(4, 'Nobin Khan', '01800000000', 'Motijheel, Dhaka.', 12, 23, 34, 45, 43, 1, 300, 450, '2017-09-18', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `p_id` int(11) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `product_price` int(11) NOT NULL,
  `product_sell` int(11) NOT NULL,
  `product_manufacture` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`p_id`, `product_id`, `product_name`, `product_image`, `product_price`, `product_sell`, `product_manufacture`) VALUES
(3, 'P#3', 'Pant', 'uploads/fe0c2b5a2f.png', 800, 0, 450);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `email`, `status`, `password`, `active`) VALUES
(1, 'S Ahmed Naim', 'naim.ahmed035@gmail.com', 'Admin', '12345', 1),
(2, 'Shakil Ahmed', 'shakil@yahoo.com', 'Customer', '12345', 2),
(3, 'Nurul Huda Sarker', 'huda@gmail.com', 'Customer', '12345', 2),
(4, 'Jhoty Sarker', 'jhoty@gmail.com', 'Customer', '12345', 1),
(5, 'Nobin Khan', 'nobin@gmail.com', 'Sells Person', '12345', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
