<?php
    include_once('inc/header.php'); 
    include("lib/User.php");
    Session::checkSession();
    $user = new User();
?>

<?php
    include "lib/Order.php";
    $order = new Order();
?>

<?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['updateOrder'])) {
        $updateOrder = $order->updateOrder($_POST, $_GET['order_id']);
    }
?>

<?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['deliveryOrder'])) {
        $deliveryOrder = $order->deliveryOrder($_GET['order_id']);
    }
?>

<?php
    if (isset($_GET['order_id'])) {
        //$getOrder = $order->getOrder($_GET['order_id']);
        $getOrderById = $order->getOrderById($_GET['order_id']);
    }
?>

<div class="panel-body">
    <!-- default navbar goes here -->
    <nav class="navbar navbar-default">
	    <div class="container-fluid">
		    <div class="navbar-header">
			    <span class="navbar-brand">
                    <h4><a style="margin-top: -10px;" href="index.php" target="_blank" class="btn btn-default">Go to Home</a></h4>
                </span>
			</div>
  		    <ul class="nav navbar-nav pull-right">
	    		<li><a><h4>Admin</h4></a></li>
		    </ul>
        </div>
    </nav>

<?php
    if (isset($updateOrder)) {
        echo $updateOrder;
    }
?>

<?php
    if (isset($deliveryOrder)) {
        echo $deliveryOrder;
    }
?>

<!-- information table goes here -->
<form action="" method="post">

<?php
    if (isset($getOrderById)) {
        foreach ($getOrderById as $value) { ?>

    <div class="form-group">
        <label for="customerName">Customer Name</label>
        <input type="text" class="form-control" id="customerName" name="customer_name" value="<?php echo $value['customer_name']; ?>" />
    </div>
    <div class="form-group">
        <label for="cell">Customer Cell</label>
        <input type="number" class="form-control" id="cell" name="customer_cell" value="<?php echo $value['customer_cell']; ?>" />
    </div>
    <div class="form-group">
        <label for="address">Address</label>
        <input type="text" class="form-control" id="address" name="customer_address" value="<?php echo $value['customer_address']; ?>" />
    </div>
    <div class="form-group">
        <label for="shoulder">Shoulder</label>
        <input type="number" class="form-control" id="shoulder" name="shoulder" value="<?php echo $value['shoulder']; ?>" />
    </div>
    <div class="form-group">
        <label for="put">Put</label>
        <input type="number" class="form-control" id="put" name="put" value="<?php echo $value['put']; ?>" />
    </div>
    <div class="form-group">
        <label for="chest">Chest</label>
        <input type="number" class="form-control" id="chest" name="chest" value="<?php echo $value['chest']; ?>" />
    </div>
    <div class="form-group">
        <label for="length">Length</label>
        <input type="number" class="form-control" id="length" name="length" value="<?php echo $value['length']; ?>" />
    </div>
    <div class="form-group">
        <label for="hand">Hand</label>
        <input type="number" class="form-control" id="hand" name="hand" value="<?php echo $value['hand']; ?>" />
    </div>
    <div class="form-group">
        <label for="quantity">Quantity</label>
        <input type="number" class="form-control" id="quantity" name="quantity" value="<?php echo $value['quantity']; ?>" />
    </div>
    <div class="form-group">
        <label for="advance">Payment</label>
        <input type="number" class="form-control" id="advance" name="payment" value="<?php echo $value['payment']; ?>" />
    </div>
    <div class="form-group">
        <label for="due">Due</label>
        <input type="number" class="form-control" id="due" name="due" 
        value="<?php 
                    $due = $value['total_cost'] - $value['payment'];
                    echo $due;
                ?>"
        />
    </div>
    <div class="form-group">
        <label for="totalCost">Total</label>
        <input type="number" class="form-control" id="totalCost" name="total_cost" value="<?php echo $value['total_cost']; ?>" />
    </div>
    <div class="form-group">
        <label for="date">Delivery Date</label>
        <input type="date" class="form-control" id="date" name="delivery_date" value="<?php echo $value['delivery_date']; ?>" />
    </div> <?php 
        }
    }
?>

    <!--
    <div class="form-group">
        <label for="profileImage">Choose profile image</label>
        <input type="file" id="profileImage">
        <p class="help-block">Click "Choose File" button to upload profile image</p>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox"> Check me out
        </label>
    </div>
    -->
    <button type="submit" class="btn btn-default" name="updateOrder">Update Order</button>
    <button type="submit" class="btn btn-default" name="deliveryOrder">Delivery Order</button>
</form>
</div>


<?php
    include_once "inc/footer.php";
?>
