<?php

	include_once 'Session.php';
	include 'Database.php';

	/**
	* 
	*/
	class User {
		private $db;

		public function __construct() {
			$this -> db = new Database();
		}

		public function userRegistration($data) {
			$username 	= $data['username'];
			$email		= $data['email'];
			$status		= $data['status'];
			$password	= $data['password'];

			$chk_email	= $this->emailCheck($email);

			if ($username == "" OR $email == "" OR $status == "" OR $password == "") {
				$msg = "<div class='alert alert-danger alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Field must not be empty!</div>";
				return $msg;
			}

			/*
			if (strlen($username) < 3) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Username is too short!</div>";
				return $msg;
			} elseif (preg_match('/[^a-z0-9_-]+/i', $username)) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Username must only contain alphanumerical, dashes and underscore!</div>";
				return $msg;
			}
			*/

			if (strlen($password) < 3) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Password must be more than 3 digit long!</div>";
				return $msg;
			}

			//$password	= md5($data['password']);

			if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Email address is not valid!</div>";
				return $msg;
			}

			if ($chk_email == true) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Email address already exists!</div>";
				return $msg;
			}

			//$password = md5($data['password']);

			$active = 0;

			$sql = "INSERT INTO TBL_USER(USERNAME, EMAIL, STATUS, PASSWORD, ACTIVE) VALUES(:username, :email, :status, :password, :active)";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':username', $username);
			$query->bindValue(':email', $email);
			$query->bindValue(':status', $status);
			$query->bindValue(':password', $password);
			$query->bindValue(':active', $active);
			$result = $query->execute();
			if ($result) {
				$msg = "<div class='alert alert-info alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success! </strong>Thank you, You registration is under approval.</div>";
				return $msg;
			} else {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Sorry! There has been problem inserting your details.</div>";
				return $msg;
			}

		}

		public function emailCheck($email) {
			$sql = "SELECT EMAIL FROM TBL_USER WHERE email = :email";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':email', $email);
			$query->execute();
			if ($query->rowCount() > 0) {
				return true;
			} else {
				return false;
			}
		}

		public function getLoginUser($email, $password) {
			$sql = "SELECT * FROM TBL_USER WHERE email = :email AND password = :password LIMIT 1";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':email', $email);
			$query->bindValue(':password', $password);
			$query->execute();
			$result = $query->fetch(PDO::FETCH_OBJ);
			return $result;
		}

		public function userLogin($data) {
			$email		= $data['email'];
			$password	= $data['password'];

			$chk_email	= $this->emailCheck($email);

			if ($email == "" OR $password == "") {
				$msg = "<div class='alert alert-danger alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Field must not be empty!</div>";
				return $msg;
			}

			if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Email address is not valid!</div>";
				return $msg;
			}

			if ($chk_email == false) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Email address not exists!</div>";
				return $msg;
			}

			$result = $this->getLoginUser($email, $password);
			
			if ($result->active == 0) {
				$msg = "<div class='alert alert-danger alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Your ID is under approval!!!</div>";
				return $msg;
			} elseif ($result->active == 1) {
				Session::init();
				Session::set("login", true);
				Session::set("id", $result->id);
				Session::set("username", $result->username);
				Session::set("status", $result->status);
				Session::set("loginmsg", "<div class='alert alert-success alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success! </strong>You are loggedIn!</div>");
				//header("Location: index.php");
				echo "<script type='text/javascript'>window.top.location='index.php';</script>";
				//echo("<script>location.href = 'index.php';</script>");
			} /*else {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Data not found!</div>";
				return $msg;
			}*/ elseif ($result->active == 2) {
				$msg = "<div class='alert alert-danger alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Sorry! </strong>You are already rejected!!!</div>";
				return $msg;
			} 

		}

		public function getUserData() {
			$sql = "SELECT * FROM TBL_USER ORDER BY ID DESC";
			$query = $this->db->pdo->prepare($sql);
			$query->execute();
			$result = $query->fetchAll();
			return $result;
		}

		public function getUserById($id) {
			$sql = "SELECT * FROM TBL_USER WHERE ID = :id LIMIT 1";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':id', $id);
			$query->execute();
			$result = $query->fetch(PDO::FETCH_OBJ);
			return $result;
		}

		public function updateUserData($id, $data) {
			$username 	= $data['username'];
			$email		= $data['email'];

			if ($username == "" OR $email == "") {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Field must not be empty!</div>";
				return $msg;
			}

			/* validation is being comment out
			if (strlen($username) < 3) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Username is too short!</div>";
				return $msg;
			} elseif (preg_match('/[^a-z0-9_-]+/i', $username)) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Username must only contain alphanumerical, dashes and underscore!</div>";
				return $msg;
			}

			if (strlen($password) < 3) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Password must be more than 3 digit long!</div>";
				return $msg;
			}

			$password	= md5($data['password']);

			if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Email address is not valid!</div>";
				return $msg;
			}

			if ($chk_email == true) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Email address already exists!</div>";
				return $msg;
			}
			*/

			$sql = "UPDATE TBL_USER SET 
						USERNAME 	= :username,
						EMAIL 		= :email
					WHERE ID 		= :id";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':username', $username);
			$query->bindValue(':email', $email);
			$query->bindValue(':id', $id);
			$result = $query->execute();
			if ($result) {
				$msg = "<div class='alert alert-success'><strong>Success! </strong>Userdata updated Successfully!</div>";
				return $msg;
			} else {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Userdata Not Updated Yet!</div>";
				return $msg;
			}
			

		}

		private function checkPassword($id, $old_pass) {
			$password = md5($old_pass);
			$sql = "SELECT PASSWORD FROM TBL_USER WHERE ID = :id AND PASSWORD = :password";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':id', $id);
			$query->bindValue(':password', $password);
			$query->execute();
			if ($query->rowCount() > 0) {
				return true;
			} else {
				return false;
			}
		}

		public function updatePassword($id, $data) {
			$old_pass = $data['old_pass'];
			$new_pass = $data['password'];
			$chk_pass = $this->checkPassword($id, $old_pass);

			if ($old_pass == "" OR $new_pass == "") {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Field must not be empty!</div>";
				return $msg;
			}

			if ($chk_pass == false) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Old Password Not Exists!</div>";
				return $msg;
			}

			if (strlen($new_pass) < 4) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Password is too short!!!</div>";
				return $msg;
			}

			$password = $new_pass;
			$sql = "UPDATE TBL_USER SET PASSWORD = :password WHERE ID = :id";
			$query = $this->db->pdo->prepare($sql);

			$query->bindValue(':password', $password);
			$query->bindValue(':id', $id);
			$result = $query->execute();
			if ($result) {
				$msg = "<div class='alert alert-success'><strong>Success! </strong>Password updated Successfully!</div>";
				return $msg;
			} else {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Password Not Updated Yet!</div>";
				return $msg;
			}



		}

		public function approveByID($id) {
	 		$sql = "UPDATE tbl_user SET `active` = 1 WHERE id = :id";
	 		$query = $this->db->pdo->prepare($sql);
	 		$query->bindParam(':id', $id);
	 		$query->execute();
	 		echo("<script>location.href = 'approve.php';</script>");
	 	}

	 	public function rejectByID($id) {
	 		$sql = "UPDATE tbl_user SET `active` = 2 WHERE id = :id";
	 		$query = $this->db->pdo->prepare($sql);
	 		$query->bindParam(':id', $id);
	 		$query->execute();
	 		echo("<script>location.href = 'approve.php';</script>");
	 	}
	 	


		
	}

?>
