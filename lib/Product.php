<?php

	include_once 'Session.php';

	/**
	* 
	*/
	class Product {
		private $db;

		public function __construct() {
			$this -> db = new Database();
		}

		public function getProduct() {
	 		$sql = "SELECT * FROM TBL_PRODUCT ORDER BY p_id DESC;";
			$query = $this->db->pdo->prepare($sql);
			$query->execute();
			$result = $query->fetchAll();
			return $result;
	 	}

		public function addProduct($data, $file) {
	 		$product_id 			= 	$data['product_id'];
	 		$product_name 			= 	$data['product_name'];
	 		$product_manufacture 	= 	$data['product_manufacture'];
	 		$product_price 			= 	$data['product_price'];
	 		$product_sell 			= 	"0";

	 		// image
            $permited  = array('jpg', 'jpeg', 'png', 'gif');
            $file_name = $file['image']['name'];
            $file_size = $file['image']['size'];
            $file_temp = $file['image']['tmp_name'];

            $div = explode('.', $file_name);
            $file_ext = strtolower(end($div));
            $unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
            $uploaded_image = "uploads/".$unique_image;

	 		if ($product_id == "" || $product_name == "" || $product_manufacture == "" || $product_price == "" || $product_sell == "" || $file_name == "") {
	 			$msg = "<div class='alert alert-danger alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Sorry! </strong>Field must not be empty!</div>";
				return $msg;
	 		} elseif ($file_size >1048567) {
                echo "<span class='error'>Image Size should be less then 1MB! </span>";
            } elseif (in_array($file_ext, $permited) === false) {
                echo "<span class='error'>You can upload only:-" .implode(', ', $permited)."</span>";
            } else {
                move_uploaded_file($file_temp, $uploaded_image);
                $sql = "INSERT INTO tbl_product(product_id, product_name, product_image, product_price, product_sell, product_manufacture) VALUES(:product_id, :product_name, :uploaded_image, :product_price, :product_sell, :product_manufacture);";
                $query = $this->db->pdo->prepare($sql);
                $query->bindValue(':product_id', $product_id);
				$query->bindValue(':product_name', $product_name);
				$query->bindValue(':uploaded_image', $uploaded_image);
				$query->bindValue(':product_price', $product_price);
				$query->bindValue(':product_sell', $product_sell);
				$query->bindValue(':product_manufacture', $product_manufacture);
				$result = $query->execute();
				$msg = "<div class='alert alert-success alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success! </strong>Product already added!</div>";
				return $msg;  
            }

	 	}

	 	public function updateProduct($data, $file, $p_id) {
	 		$product_id 			= 	$data['product_id'];
	 		$product_name 			= 	$data['product_name'];
	 		$product_manufacture 	= 	$data['product_manufacture'];
	 		$product_price 			= 	$data['product_price'];
	 		$product_sell 			= 	$data['product_sell'];
	 		//$product_sell 			= 	"0";

	 		// image
            $permited  = array('jpg', 'jpeg', 'png', 'gif');
            $file_name = $file['image']['name'];
            $file_size = $file['image']['size'];
            $file_temp = $file['image']['tmp_name'];

            $div = explode('.', $file_name);
            $file_ext = strtolower(end($div));
            $unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
            $uploaded_image = "uploads/".$unique_image;

            if ($product_id == "" || $product_name == "" || $product_manufacture == "" || $product_price == "" || $product_sell == "") {
	 			$msg = "<div class='alert alert-danger alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Sorry! </strong>Field must not be empty!</div>";
				return $msg;
	 		} else {
	 			if (!empty($file_name)) {
                    if ($file_size > 1048567) {
                        echo "<span class='error'>Image Size should be less then 1MB! </span>";
                    } elseif (in_array($file_ext, $permited) === false) {
                        echo "<span class='error'>You can upload only:- " . implode(', ', $permited) . "</span>";
                    } else {
						// unlink previous image from source
						$this->productImageUnlink($p_id);

						// upload new image from user
                        move_uploaded_file($file_temp, $uploaded_image);

                        $sql = "UPDATE tbl_product 
                        		SET 
                        		product_id			= :product_id,
                        		product_name 		= :product_name,
								product_image 		= :uploaded_image,
								product_price		= :product_price,
								product_sell		= :product_sell,
								product_manufacture	= :product_manufacture
								WHERE 
								p_id				= :p_id;
						";
		                $query = $this->db->pdo->prepare($sql);
		                $query->bindValue(':product_id', $product_id);
						$query->bindValue(':product_name', $product_name);
						$query->bindValue(':uploaded_image', $uploaded_image);
						$query->bindValue(':product_price', $product_price);
						$query->bindValue(':product_sell', $product_sell);
						$query->bindValue(':product_manufacture', $product_manufacture);
						$query->bindValue(':p_id', $p_id);
						$result = $query->execute();
						$msg = "<div class='alert alert-success alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success! </strong>Product already updated!</div>";
						return $msg;  
                    }
                } else {
                	move_uploaded_file($file_temp, $uploaded_image);
                    $sql = "UPDATE tbl_product 
                        		SET 
                        		product_id			= :product_id,
                        		product_name 		= :product_name,
								product_price		= :product_price,
								product_sell		= :product_sell,
								product_manufacture	= :product_manufacture
								WHERE
								p_id 				= :p_id;
						";
	                $query = $this->db->pdo->prepare($sql);
	                $query->bindValue(':product_id', $product_id);
					$query->bindValue(':product_name', $product_name);
					$query->bindValue(':product_price', $product_price);
					$query->bindValue(':product_sell', $product_sell);
					$query->bindValue(':product_manufacture', $product_manufacture);
					$query->bindValue(':p_id', $p_id);
					$result = $query->execute();
					$msg = "<div class='alert alert-success alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success! </strong>Product already updated!</div>";
					return $msg;  
                }
                
	 		}





	 	}

	 	public function detailsProduct($p_id) {
	 		$sql = "SELECT * FROM TBL_PRODUCT WHERE p_id = :p_id;";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':p_id', $p_id);
			$query->execute();
			$result = $query->fetchAll();
			return $result;
	 	}

	 	public function deleteProduct($p_id) {
	 		// unlink previous image from source
			$this->productImageUnlink($p_id);

			$sql = "DELETE FROM TBL_PRODUCT WHERE p_id = :p_id;";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':p_id', $p_id);
			$query->execute();
			
			//$msg = "<div class='alert alert-success alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success! </strong>Product deleted successfully!</div>";
			//return $msg;
			echo "<script type='text/javascript'>window.top.location='index.php';</script>";
	 	}

	 	public function productImageUnlink($p_id) {
	 		$sql = "SELECT * FROM TBL_PRODUCT WHERE p_id = :p_id;";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':p_id', $p_id);
			$query->execute();
			$result = $query->fetch(PDO::FETCH_OBJ);

			if ($result) {
				$deleteImage = $result->product_image;
				unlink($deleteImage);
			}
	 	}






	}


?>
