<?php
	include_once 'Session.php';

	/**
	* 
	*/
	class Order	{
		private $db;

		public function __construct() {
			$this -> db = new Database();
		}

		public function getOrder() {
			$sql = "SELECT * FROM TBL_ORDER ORDER BY order_id DESC;";
			$query = $this->db->pdo->prepare($sql);
			$query->execute();
			$result = $query->fetchAll();
			return $result;
		}

		public function makeOrder($data) {
			$customer_name		= $data['customer_name'];
			$customer_cell 		= $data['customer_cell'];
			$customer_address	= $data['customer_address'];
			$shoulder 			= $data['shoulder'];
			$put 				= $data['put'];
			$chest 				= $data['chest'];
			$length 			= $data['length'];
			$hand 				= $data['hand'];
			$quantity 			= $data['quantity'];
			$payment 			= $data['payment'];
			$total_cost 		= $data['total_cost'];
			$delivery_date 		= $data['delivery_date'];
			$delivery_order		= "0";

			if ($customer_name == "" OR $customer_cell == "" OR $customer_address == "" OR $shoulder == "" OR $put == "" OR $chest == "" OR $length == "" OR $hand == "" OR $quantity == "" OR $payment == "" OR $total_cost == "" OR $delivery_date == "") {
				$msg = "<div class='alert alert-danger alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Sorry! </strong>Field must not be empty!</div>";
				return $msg;
			} else {
				$sql = "INSERT INTO tbl_order(customer_name, customer_cell, customer_address, shoulder, put, chest, length, hand, quantity, payment, total_cost, delivery_date, delivery_order) VALUES(:customer_name, :customer_cell, :customer_address, :shoulder, :put, :chest, :length, :hand, :quantity, :payment, :total_cost, :delivery_date, :delivery_order);";
				$query = $this->db->pdo->prepare($sql);
				$query->bindValue(':customer_name', $customer_name);
				$query->bindValue(':customer_cell', $customer_cell);
				$query->bindValue(':customer_address', $customer_address);
				$query->bindValue(':shoulder', $shoulder);
				$query->bindValue(':put', $put);
				$query->bindValue(':chest', $chest);
				$query->bindValue(':length', $length);
				$query->bindValue(':hand', $hand);
				$query->bindValue(':quantity', $quantity);
				$query->bindValue(':payment', $payment);
				$query->bindValue(':total_cost', $total_cost);
				$query->bindValue(':delivery_date', $delivery_date);
				$query->bindValue(':delivery_order', $delivery_order);
				$result = $query->execute();
				$msg = "<div class='alert alert-success alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success! </strong>Order make successfully!</div>";
				return $msg;
			}


		}

		public function updateOrder($data, $order_id) {
			$customer_name		= $data['customer_name'];
			$customer_cell 		= $data['customer_cell'];
			$customer_address	= $data['customer_address'];
			$shoulder 			= $data['shoulder'];
			$put 				= $data['put'];
			$chest 				= $data['chest'];
			$length 			= $data['length'];
			$hand 				= $data['hand'];
			$quantity 			= $data['quantity'];
			$payment 			= $data['payment'];
			$total_cost 		= $data['total_cost'];
			$delivery_date 		= $data['delivery_date'];

			if ($customer_name == "" OR $customer_cell == "" OR $customer_address == "" OR $shoulder == "" OR $put == "" OR $chest == "" OR $length == "" OR $hand == "" OR $quantity == "" OR $payment == "" OR $total_cost == "" OR $delivery_date == "") {
				$msg = "<div class='alert alert-danger alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Sorry! </strong>Field must not be empty!</div>";
				return $msg;
			} else {
				$sql = "UPDATE tbl_order
						SET 
						customer_name = :customer_name,
						customer_cell = :customer_cell,
						customer_address = :customer_address,
						shoulder = :shoulder,
						put = :put,
						chest = :chest,
						length = :length,
						hand = :hand,
						quantity = :quantity,
						payment = :payment,
						total_cost = :total_cost,
						delivery_date = :delivery_date
						WHERE 
						order_id = :order_id;
				";
				$query = $this->db->pdo->prepare($sql);
				$query->bindValue(':customer_name', $customer_name);
				$query->bindValue(':customer_cell', $customer_cell);
				$query->bindValue(':customer_address', $customer_address);
				$query->bindValue(':shoulder', $shoulder);
				$query->bindValue(':put', $put);
				$query->bindValue(':chest', $chest);
				$query->bindValue(':length', $length);
				$query->bindValue(':hand', $hand);
				$query->bindValue(':quantity', $quantity);
				$query->bindValue(':payment', $payment);
				$query->bindValue(':total_cost', $total_cost);
				$query->bindValue(':delivery_date', $delivery_date);
				$query->bindValue(':order_id', $order_id);
				$result = $query->execute();
				$msg = "<div class='alert alert-success alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success! </strong>Customer order updated successfully!</div>";
				return $msg;
			}

		}

		public function getOrderById($order_id) {
			$sql = "SELECT * FROM tbl_order WHERE order_id = :order_id LIMIT 1;";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':order_id', $order_id);
			$query->execute();
			$result = $query->fetchAll();
			return $result;
		}

		public function deliveryOrder($order_id) {
			$result = $this->getOrderById($order_id);
			if ($result) {
				foreach ($result as $value) {
					$total_cost = $value['total_cost'];
				}
			}
			
			$payment = $total_cost;
			
			$sql = "UPDATE tbl_order
					SET 
					payment		= :payment,
					delivery_order	= '1'
					WHERE 
					order_id = :order_id;
			";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':payment', $payment);
			$query->bindValue(':order_id', $order_id);
			$result = $query->execute();
			//echo "<script type='text/javascript'>window.top.location='order.php';</script>";
			$msg = "<div class='alert alert-success alert-dismissable fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Congratulation! </strong>Product delivered to customer successfully!</div>";
			return $msg;
		}


	}


?>