<?php
    include_once('inc/header.php'); 
    include("lib/User.php");
    Session::checkSession();
    $user = new User();
?>

<?php
    include "lib/Order.php";
    $order = new Order();
?>

<?php
    if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['makeOrder'])) {
        $makeOrder = $order->makeOrder($_POST);
    }
    
?>

<div class="panel-body">
    <!-- default navbar goes here -->
    <nav class="navbar navbar-default">
	    <div class="container-fluid">
		    <div class="navbar-header">
			    <span class="navbar-brand">
                    <h4><a style="margin-top: -10px;" href="index.php" target="_blank" class="btn btn-default">Go to Home</a></h4>
                </span>
			</div>
  		    <ul class="nav navbar-nav pull-right">
	    		<li><a><h4>Admin</h4></a></li>
		    </ul>
        </div>
    </nav>

<?php
    if (isset($makeOrder)) {
        echo $makeOrder;
    }
?>

<!-- information table goes here -->
<form action="" method="post">
    <div class="form-group">
        <label for="customerName">Customer Name</label>
        <input type="text" class="form-control" id="customerName" name="customer_name" placeholder="Type Customer name here. . ."/>
    </div>
    <div class="form-group">
        <label for="cell">Customer Cell</label>
        <input type="number" class="form-control" id="cell" name="customer_cell" placeholder="Type customer cell here. . ."/>
    </div>
    <div class="form-group">
        <label for="address">Address</label>
        <input type="text" class="form-control" id="address" name="customer_address" placeholder="Type customer address here. . ."/>
    </div>
    <div class="form-group">
        <label for="shoulder">Shoulder</label>
        <input type="number" class="form-control" id="shoulder" name="shoulder" placeholder="Type shoulder here. . ."/>
    </div>
    <div class="form-group">
        <label for="put">Put</label>
        <input type="number" class="form-control" id="put" name="put" placeholder="Type put here. . ."/>
    </div>
    <div class="form-group">
        <label for="chest">Chest</label>
        <input type="number" class="form-control" id="chest" name="chest" placeholder="Type chest here. . ."/>
    </div>
    <div class="form-group">
        <label for="length">Length</label>
        <input type="number" class="form-control" id="length" name="length" placeholder="Type length here. . ."/>
    </div>
    <div class="form-group">
        <label for="hand">Hand</label>
        <input type="number" class="form-control" id="hand" name="hand" placeholder="Type hand here. . ."/>
    </div>
    <div class="form-group">
        <label for="quantity">Quantity</label>
        <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Type shoulder here. . ."/>
    </div>
    <div class="form-group">
        <label for="payment">Advance</label>
        <input type="number" class="form-control" id="payment" name="payment" placeholder="Type advance amount here. . ."/>
    </div>
    <div class="form-group">
        <label for="cost">Total Cost</label>
        <input type="number" class="form-control" id="cost" name="total_cost" placeholder="Type total cost here. . ."/>
    </div>
    <div class="form-group">
        <label for="date">Delivery Date</label>
        <input type="date" class="form-control" id="date" name="delivery_date" placeholder="Type delivery date here. . ."/>
    </div>

    <!--
    <div class="form-group">
        <label for="profileImage">Choose profile image</label>
        <input type="file" id="profileImage">
        <p class="help-block">Click "Choose File" button to upload profile image</p>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox"> Check me out
        </label>
    </div>
    -->
    <button type="submit" class="btn btn-default" name="makeOrder">Make Order</button>
</form>
</div>


<?php
    include_once "inc/footer.php";
?>
