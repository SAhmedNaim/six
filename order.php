<?php
    include_once('inc/header.php'); 
    include("lib/User.php");
    Session::checkSession();
    $user = new User();
?>

<?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['search'])) {
        $searchData = $search->search("tbl_order", "customer_name", $_POST['keyword']);
    }
?>

<?php
    include "lib/Order.php";
    $order = new Order();
?>

    <div class="panel-body">
        <!-- default navbar goes here -->
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <span class="navbar-brand"><h4><a style="margin-top: -10px;" href="addorder.php" target="_blank" class="btn btn-default">Add New Order</a></h4></span>
                </div>
                <ul class="nav navbar-nav pull-right" style="width: 10%; margin-top: -10px; padding-bottom: 0px;">

                
                <span class="navbar-brand"><h4><a style="margin-right: 00px; " href="index.php" target="_blank" class="btn btn-default">Go to Home</a></h4></span>





                    <li><a>
                        <!-- Searchbar code goes here --><!--                                           
                        <input type="text" name="" placeholder="Seach product here. . ." style="width: 290px !important; margin: 5px;">-->
                        <!--
                        <form action="search.php" method="post">    
                            <div class="col-lg-6">
                                <div class="input-group" style=" margin-bottom: 15px;">
                                    <input type="text" class="form-control" name="keyword" placeholder="Search product here. . ." style="width:320px !important;" />
                                    <span class="input-group-btn">
                                        <input type="submit" class="btn btn-default" name="search" value="Search" />
                                    </span>
                                </div>
                            </div>
                        </form>
                        -->
                        <!-- Searchbar code collapse -->




                    </a></li>
                </ul>
            </div>
        </nav>
        
        <!-- information table goes here -->
        <table class="table table-striped table-bordered">
            <th width="10%">Serial</th>
            <th width="20%">Customer Name</th>
            <th width="10%">Payment</th>
            <th width="10%">Due</th>
            <th width="10%">Total Cost</th>
            <th width="10%">Delivery Date</th>
            <th width="10%">Action</th>
            
<?php
    $getOrder = $order->getOrder();
    if ($getOrder) {
        $i = 0;
        foreach ($getOrder as $value) { 
            $i++; ?>

            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $value['customer_name']; ?></td>
                <td><?php echo $value['payment']; ?></td>
                <td>
                    <?php 
                        $due = $value['total_cost'] - $value['payment'];
                        echo $due;
                    ?>
                </td>
                <td><?php echo $value['total_cost']; ?></td>
                <td><?php echo $value['delivery_date']; ?></td>
                <td>
                    <!--<a class="btn btn-primary" href="profile.php?id=1">View</a>-->
                    <!-- Split button -->
                    <div class="btn-group">
                        <button type="button" class="btn btn-default">
                            <?php
                                $delivery_order = $value['delivery_order'];
                                if ($delivery_order == "0") {
                                    echo "Pending";
                                } elseif ($delivery_order == "1") {
                                    echo "Delivered";
                                }
                            ?>
                      </button>
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu">
                        <li>
                            <a href="detailsorder.php?order_id=<?php echo $value['order_id']; ?>"target="_blank"><span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> Details</a>
                        </li>
                        <li>
                            <a href="#" onclick="return confirm('Are you sure to delete data?');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
                        </li>
                                        <!-- 
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        -->
                      </ul>
                    </div>
                </td>
            </tr> <?php 
        }
    } else { ?>
            <tr>
                <td colspan="6" style="letter-spacing: 4px;"><h2>Order Not Found</h2></td>
            </tr> <?php
    }
    
?>

        </table>

    </div>

    
<?php
    include_once "inc/footer.php";
?>
