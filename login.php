<?php
    include 'inc/header.php';
    include 'lib/User.php';
    Session::checkLogin();
?>

<?php
    $user = new User();
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['login'])) {
        $userLogin = $user->userLogin($_POST);
    }
?>

<div class="panel-heading">
    <h2>User Login</h2>
</div>

<div class="panel-body">

<?php
    if (isset($userLogin)) {
        echo $userLogin;
    }
?>

    <div class="row">
        <div class="col-md-4">
            <img src="inc/img/logo.jpg" class="img-responsive" alt="Company Logo" style="height: 180px; width: 350px;" />
        </div>
        <div class="col-md-8">
            <div style="max-width: 600px; margin: 0px auto;">
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                    <div class="form-group">
                        <label for="email">Email Address</label>
                        <input type="text" id="email" name="email" class="form-control" placeholder="Email" style="min-width:100%" />
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" style="min-width:100%" />
                    </div>
                    <button type="submit" name="login" class="btn btn-default" style="min-width: 15%;">Login</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
    include_once "inc/footer.php";
?>
