<?php
	include_once('inc/header.php'); 
	include("lib/User.php");
	Session::checkSession();
	$user = new User();
?>

<?php
	if (isset($_GET['action']) && $_GET['action'] == 'approve') {
		$id = (int)$_GET['id'];
		$approve = $user->approveByID($id);
	}
?>

<?php
	if (isset($_GET['action']) && $_GET['action'] == 'reject') {
		$id = (int)$_GET['id'];
		$reject = $user->rejectByID($id);
	}
?>

	<div class="panel-body">
		<!-- default navbar goes here -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<span class="navbar-brand"><h4>User List</h4></span>
				</div>
				<ul class="nav navbar-nav pull-right" style="width: 40%;">
					<li><a>

						<!-- Searchbar code goes here --><!--											
						<input type="text" name="" placeholder="Seach product here. . ." style="width: 290px !important; margin: 5px;">-->

						<div class="col-lg-6">
						    <div class="input-group" style=" margin-bottom: 15px;">
						      	<input type="text" class="form-control" placeholder="Search user here. . ." style="width:320px !important;">
						      	<span class="input-group-btn">
						        	<button class="btn btn-default" type="button">Search</button>
						     	</span>
						    </div><!-- /input-group -->
  						</div><!-- /.col-lg-6 -->



					</a></li>
				</ul>
			</div>
		</nav>
		
		<!-- information table goes here -->
		<table class="table table-striped table-bordered">
			<th width="10%">Serial</th>
			<th width="20%">Username</th>
			<th width="30%">Email</th>
			<th width="20%">Status</th>
			<th width="20%">Approval</th>
			
			<?php
				$getUserData = $user->getUserData();
				if ($getUserData) {
					$i = 0;
					foreach ($getUserData as $value) {
						$i++; ?>

			<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $value['username']; ?></td>
				<td><?php echo $value['email']; ?></td>
				<td><?php echo $value['status']; ?></td>
				<td>
					<!--<a class="btn btn-primary" href="profile.php?id=1">View</a>-->
					<!-- Split button -->
					<div class="btn-group">
					  <button type="button" class="btn btn-default">
					 		<?php
								if ($value['active'] == 0) {
									echo "Pending";
								} elseif($value['active'] == 1) {
									echo "Approved";
								} elseif($value['active'] == 2) {
									echo "Rejected";
								}
							?>
					  </button>
					  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    <span class="caret"></span>
					    <span class="sr-only">Toggle Dropdown</span>
					  </button>
					  <ul class="dropdown-menu">
						<?php
							if ($value['active'] == 0) { ?>
								<li>
						    		<a href="approve.php?action=approve&id=<?php echo $value['id']; ?>" onclick="return confirm('Are you sure to approve?');"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Approve</a>
						    	</li>
						  		<li>
						  			<a href="approve.php?action=reject&id=<?php echo $value['id']; ?>" onclick="return confirm('Are you sure to reject user?');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Reject</a>
						    	</li> <?php
							} elseif ($value['active'] == 1) { ?>
								<?php
									if (Session::get('id') == $value['id']) { ?>
										<li>
											<a href="profile.php" target="_blank"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Profile</a>
										</li> <?php
									} else { ?>
										<li>
						  					<a href="approve.php?action=reject&id=<?php echo $value['id']; ?>" onclick="return confirm('Are you sure to reject user?');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Reject</a>
						    			</li> <?php
						    		}
								?> <?php
						    } elseif ($value['active'] == 2) { ?>
						    	<li>
						    		<a href="approve.php?action=approve&id=<?php echo $value['id']; ?>" onclick="return confirm('Are you sure to approve?');"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Approve</a>
						    	</li> <?php 
						    }
						?>	
				  	

				  						<!-- 
					    <li><a href="#">Something else here</a></li>
					    <li role="separator" class="divider"></li>
					    <li><a href="#">Separated link</a></li>
					    -->
					  </ul>
					</div>
				</td>
			</tr> <?php
					}
				}
			?>

		</table>

	</div>

	
<?php
    include_once "inc/footer.php";
?>
