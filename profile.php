<?php
    include_once('inc/header.php'); 
    include("lib/User.php");
    Session::checkSession();
    $user = new User();
?>

<?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update'])) {
        $id = Session::get('id');
        $updateUserData = $user->updateUserData($id, $_POST);
    }
?>

<div class="panel-body">
		<!-- default navbar goes here -->
		<nav class="navbar navbar-default">
		    <div class="container-fluid">
			      <div class="navbar-header">
				  	    <span class="navbar-brand">
                    <h4>User Profile</h4>
                </span>
				    </div>
				    <ul class="nav navbar-nav pull-right">
					      <li><a><h4><a style="margin-top: -30px;" href="index.php" target="_blank" class="btn btn-default">Go to Home</a></h4></a></li>
				    </ul>
			  </div>
		</nav>

        <?php
            if (isset($updateUserData)) {
                echo $updateUserData;
            }
        ?>
		
		<!-- information table goes here -->
		<form action="" method="post">
        <?php
            $id = Session::get("id");
            $getUserById = $user->getUserById($id);
            if ($getUserById) { ?>
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="<?php echo $getUserById->username; ?>"/>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="<?php echo $getUserById->email; ?>"/>
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <input type="text" class="form-control" id="status" name="status" value="<?php echo $getUserById->status; ?>"" disabled/>
                </div>

                <!--
                <div class="form-group">
                    <label for="profileImage">Choose profile image</label>
                    <input type="file" id="profileImage">
                    <p class="help-block">Click "Choose File" button to upload profile image</p>
                </div>
                -->
                <!--
                <div class="checkbox">
                    <label>
                    <input type="checkbox"> Check me out
                    </label>
                </div>
                -->
                <?php
            }
        ?>
                    <button type="submit" class="btn btn-default" name="update">Update Profile</button>
    </form>
</div>


<?php
    include_once "inc/footer.php";
?>
