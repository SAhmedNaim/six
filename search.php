<?php
	include_once('inc/header.php'); 
	include("lib/User.php");
	Session::checkSession();
	$user = new User();
?>

<?php
	include "lib/Product.php";
	$product = new Product();
?>

<?php
	include "lib/Search.php";
	$search = new Search();
?>

<?php
	if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['search'])) {
		$searchData = $search->search("tbl_product", "product_id", $_POST['keyword']);
	}
?>

<?php
	if (isset($_GET['action']) && $_GET['action'] == 'delete') {
		$deleteProduct = $product->deleteProduct($_GET['p_id']);
	}
?>

	<div class="panel-body">
		<!-- default navbar goes here -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<span class="navbar-brand"><h4><a style="margin-top: -10px;" href="addproduct.php" target="_blank" class="btn btn-default">Add New Product</a></h4></span>
				</div>
				<ul class="nav navbar-nav pull-right" style="width: 40%;">
					<li><a>

						<!-- Searchbar code goes here --><!--											
						<input type="text" name="" placeholder="Seach product here. . ." style="width: 290px !important; margin: 5px;">-->
<form action="search.php" method="post">	
	<div class="col-lg-6">
	    <div class="input-group" style=" margin-bottom: 15px;">
	  		<input type="text" class="form-control" name="keyword" placeholder="Search product here. . ." style="width:320px !important;" />
		    <span class="input-group-btn">
	        	<input type="submit" class="btn btn-default" name="search" value="Search" />
	     	</span>
	    </div><!-- /input-group -->
	</div><!-- /.col-lg-6 -->
</form>
						<!-- Searchbar code collapse -->
						
					</a></li>
				</ul>
			</div>
		</nav>
		
		<!-- information table goes here -->
		<table class="table table-striped table-bordered">
			<th width="10%">Product ID</th>
			<th width="20%">Product Name</th>
			<th width="20%">Product Image</th>
			<th width="10%">Price</th>
			<th width="10%">Total Sell</th>
			<th width="10%">Action</th>
			
			<?php
				if (!empty($searchData)) {
					foreach ($searchData as $value) { ?>

			<tr>
				<td><?php echo $value['product_id']; ?></td>
				<td><?php echo $value['product_name']; ?></td>
				<td><img src="<?php echo $value['product_image']; ?>" width="200px" height="30px" alt="Product Image Missing"></td>
				<td><?php echo $value['product_price']; ?></td>
				<td><?php echo $value['product_sell']; ?></td>
				<td>
					<!--<a class="btn btn-primary" href="profile.php?id=1">View</a>-->
					<!-- Split button -->
					<div class="btn-group">
					  <button type="button" class="btn btn-default">Action</button>
					  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    <span class="caret"></span>
					    <span class="sr-only">Toggle Dropdown</span>
					  </button>
					  <ul class="dropdown-menu">
				    	<li>
				    		<a href="detailsproduct.php?p_id=<?php echo $value['p_id']; ?>"target="_blank"><span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> Details</a>
				    	</li>
				  		<li>
				  			<a href="index.php?action=delete&p_id=<?php echo $value['p_id']; ?>" onclick="return confirm('Are you sure you want to delete data?');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
				    	</li>
				  						<!-- 
					    <li><a href="#">Something else here</a></li>
					    <li role="separator" class="divider"></li>
					    <li><a href="#">Separated link</a></li>
					    -->
					  </ul>
					</div>
				</td>
			</tr> <?php
					}
				} elseif(empty($searchData) OR !isset($searchData)) { ?>
					<tr>
						<td colspan="6" style="letter-spacing: 4px;"><h2>Data Not Found</h2></td>
					</tr> <?php
				}
			?>

		</table>

	</div>
	
<?php
    include_once "inc/footer.php";
?>
