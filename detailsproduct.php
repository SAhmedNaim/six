<?php
    include_once('inc/header.php'); 
    include("lib/User.php");
    Session::checkSession();
    $user = new User();
?>

<?php
    include "lib/Product.php";
    $product = new Product();
?>

<?php
    if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['updateProduct'])) {
        $updateProduct = $product->updateProduct($_POST, $_FILES, $_GET['p_id']);
    }
?>

<?php
    if (isset($_GET['p_id'])) {
        $detailsProduct = $product->detailsProduct($_GET['p_id']);
    }
?>

<div class="panel-body">
	<!-- default navbar goes here -->
	<nav class="navbar navbar-default">
		  <div class="container-fluid">
			    <div class="navbar-header">
				      <span class="navbar-brand">
                <h4><a style="margin-top: -10px;" href="index.php" target="_blank" class="btn btn-default">Go to Home</a></h4>
            </span>
			    </div>
			    <ul class="nav navbar-nav pull-right">
				      <li><a><h4>Admin</h4></a></li>
			    </ul>
		  </div>
	</nav>

    <?php
        if (isset($updateProduct)) {
            echo $updateProduct;
        }
    ?>
	
	<!-- information table goes here -->
	<form action="" method="post" enctype="multipart/form-data">
    <?php
        if ($detailsProduct) {
            foreach ($detailsProduct as $value) { ?>
                <div class="form-group">
                    <label for="productID">Product ID</label>
                    <input type="text" class="form-control" id="productID" name="product_id" value="<?php echo $value['product_id']; ?>" />
                </div>
                <div class="form-group">
                    <label for="productName">Product Name</label>
                    <input type="text" class="form-control" id="productName" name="product_name" value="<?php echo $value['product_name']; ?>"/>
                </div>
                <div class="form-group">
                    <label for="productManufacturingCost">Manufacturing Cost</label>
                    <input type="number" class="form-control" id="exampleInputEmail1" name="product_manufacture" value="<?php echo $value['product_manufacture']; ?>"/>
                </div>
                <div class="form-group">
                    <label for="productPrice">Product Price</label>
                    <input type="number" class="form-control" id="productPrice" name="product_price" value="<?php echo $value['product_price']; ?>"/>
                </div>
                <div class="form-group">
                    <label for="productSell">Product Sell</label>
                    <input type="number" class="form-control" id="productSell" name="product_sell" value="<?php echo $value['product_sell']; ?>"/>
                </div>
                <div class="form-group">
                    <label for="productImage">Product Image</label>
                    <br/>
                    <img src="<?php echo $value['product_image']; ?>" alt="Product image missing!" width="100%" height="400px" style="padding-bottom: 7px;" />
                    <input type="file" id="productImage" name="image" />
                    <p class="help-block">Click "Choose File" button to upload product image</p>
                </div>
                <!--
                <div class="checkbox">
                    <label>
                        <input type="checkbox"> Check me out
                    </label>
                </div>
                -->
                <?php
            }
        }
   ?>
                <button type="submit" class="btn btn-default" name="updateProduct">Update Product</button>
    </form>
</div>


<?php
    include_once "inc/footer.php";
?>
